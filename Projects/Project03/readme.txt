Callie Monroe
Project 03
21 November 2020
This website will be a portfolio of my work from my ASEM class during my junior year at DU.

index.html is a home page which provides a final reflection of the course and a navigation feature to other portions of the site. Navigation feature uses javascript to create a slideshow effect

aboutme.html features a brief about me section, a picture of my family, a blurb about my political identity and my ethnic identity and one of my political passions (women's rights). Each blurb is followed by a paper I wrote to support my claims. Read more button included in about me section

readingreflections.html takes a look at three assignments I completed during the course and provides a brief synopsis of each paper before attaching said paper underneath it. Each assignment can be seen by clicking on a drop down button

filmreflections.html reflects on two films we watched in class. This page includes two papers and one clip from a documentary

mran.html is my page focusing on my reaction to the book Making Race and Nation by Anthony W. Marx. It includes a picture of the book, a short overview of my work related to the book, and a link to my work

contact.html features a table of my contact information & a form to send me an email. Submit button uses javascript to send user confirmation alert

script.js implements features stated above

style.css defines style elements for the whole website and individual pages

I used w3schools.com to help with various javascript implementation
