//for contact.html
function confirmFunc() {
  confirm("Are you sure you would like to submit?");
}

//for slideshow navigation on index.html
var navIndex = 0;
showNav(navIndex);

function showNav(x) {
  var navs = document.getElementsByClassName("webpagenav");

  if (x > navs.length) {
    navIndex = 1;
  }
  if (x < 1) {
    navIndex = navs.length;
  }
  for (var i = 0; i < navs.length; i++) {
    navs[i].style.display = "none";
  }
  navs[navIndex - 1].style.display = "block";
}

function nextNav(x) {
  showNav((navIndex += x));
}

function currentNav(x) {
  showNav((navIndex = x));
}

//read more of bio in aboutme.html
function readMore() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("readmore");
  var btnText = document.getElementById("button");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    moreText.style.display = "inline";
  }
}

//collapse/expand assignment blurb in readingreflections.html
function openAssignment() {
  var assignment = document.getElementsByClassName("assignment");
  var i;

  for (i = 0; i < assignment.length; i++) {
    assignment[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var content = this.nextElementSibling;
      if (content.style.display === "block") {
        content.style.display = "none";
      } else {
        content.style.display = "block";
      }
    });
  }
}
