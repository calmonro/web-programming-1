/* Callie Monroe
 lab11.js
 10-26-2020 */
export class Lab11 {
  testDefaultParameters(a, b = 100) {
    var jsonObject = {
      firstAttribute: a,
      secondAttribute: b
    };
    return jsonObject;
  }

  testTemplateLiterals(firstName, middleName, lastName) {
    var stringTemplate = `${firstName}, ${middleName}, ${lastName}`;
    return stringTemplate;
  }

  testMultilineStrings() {
    var multilineString = `My dog likes Haikus
  Which is weird since he's a dog
  He's smarter than me
  -Callie Monroe`;
    return multilineString;
  }

  testSortWithArrowFunction(arrNums) {
    arrNums.sort((a, b) => b - a);
  }
}

let lab = new Lab11();

let testParam = lab.testDefaultParameters(200);
console.log(testParam.first);
console.log(testParam.second);

testParam = lab.testDefaultParameters(200, 300);
console.log(testParam.first);
console.log(testParam.second);

let testTemplate = lab.testTemplateLiterals("Callie", "Fox", "Monroe");
console.log(testTemplate);

let testMultiline = lab.testMultilineStrings();
console.log(testMultiline);

let arrNums = [4, 2, 10, 23, 4];
let testSort = lab.testSortWithArrowFunction(arrNums);
console.log(arrNums);
console.log(testSort);
